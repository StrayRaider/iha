import time
import sys
# Import mavutil
from pymavlink import mavutil
master = mavutil.mavlink_connection('/dev/ttyACM0')
# Wait a heartbeat before sending commands
master.wait_heartbeat()


def take_message(msg_type = None):
#self.connection.mav.ping_send(int(time.time() * 1e6),0,0,0)
    try:
        if msg_type != None:
            msg = master.recv_match(type = msg_type, blocking=True)
        else:
            msg = master.recv_match(blocking=True)
        return msg
    except:
        print("message did not taked")


for i in master.messages:
    print(i,"\n")
    print(master.messages[i],"\n")
"""
while 1:
    x = take_message("GPS_RAW_INT")
    print(master.messages["GPS_RAW_INT"].lat,master.messages["GPS_RAW_INT"].lon )

"""
