import gi, os
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, Gdk, GObject, GdkPixbuf
import math

class Battery(Gtk.DrawingArea):
    def __init__(self):
        Gtk.DrawingArea.__init__(self)

        #paint area sizeı
        self.size = (115,202)
        #paint area size atama
        self.set_size_request(*self.size)
        self.cursor_size = (142,142)
        self.cursor_pos = (-self.cursor_size[0]/2,-self.cursor_size[1]/2)
        self.back_size = (115,202)
        self.back_pos = [0,0]
        self.connect("draw", self.draw_all)
        self.cursor_angle = 0
        self.queue_draw()

    def draw_all(self,widget,cr):
        #akimper_c dosyasını değişkene atadık
        img_path = os.path.expanduser("./assets/akimper_c.png")
        #akımper_c dosyasını Gdk pixbuf ile draw area ya çizilevbilir hale getirdik
        self.cursor = GdkPixbuf.Pixbuf.new_from_file(img_path)
        img_path = os.path.expanduser("./assets/batarya.png")
        self.back = GdkPixbuf.Pixbuf.new_from_file(img_path)
       
        #back dosyasını draw area ya ekledik
        Gdk.cairo_set_source_pixbuf(cr,self.back,*self.back_pos)
        #back dosyasını çizdik
        cr.paint()
        #fotoğraf paint area nın 0,0 noktasından olması gereken noktaya taşındı
        cr.translate(self.size[0]-20, self.size[1]/2)
        #fotoğraf radyan cinsinde aldığı veriyle döndürüldü
        cr.rotate(math.pi/180*self.cursor_angle)
        #fotoğraf draw area ya eklendi
        Gdk.cairo_set_source_pixbuf(cr,self.cursor,*self.cursor_pos)
        # çizildi
        cr.paint()

