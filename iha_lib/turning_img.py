import gi, os
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, Gdk, GObject, GdkPixbuf

class Turn_Img(Gtk.DrawingArea):
    def __init__(self):
        Gtk.DrawingArea.__init__(self)

        self.set_size_request(200,200)
        self.image_size = 100#kare (100,100)
        self.pos = (0,0)
        self.connect("draw", self.draw_all)
        self.queue_draw()

    def draw_all(self,widget,cr):
        img_path = os.path.expanduser("./asstes/plane_0.png")
        self.image = GdkPixbuf.Pixbuf.new_from_file(img_path)#,self.image_size,self.image_size)
       
        
        cr.translate(self.image_size, self.image_size)
        cr.rotate(math.pi/180*self.angle)
        Gdk.cairo_set_source_pixbuf(cr,self.image,self.pos[0],self.pos[1])
        cr.paint()
