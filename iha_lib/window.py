import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, Gdk
from iha_lib import connect
from iha_lib import indicator as indi
from iha_lib import pitch
from iha_lib import yaw
from iha_lib import roll
from iha_lib import velo
from iha_lib import current
from iha_lib import battery
from iha_lib import wp
from iha_lib import settings

class My_Window(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        s = Gdk.Screen.get_default()
        self.set_size_request(s.get_width(), s.get_height())
        self.set_size_request(*settings.Screen_Size)
        self.anakutu = Gtk.VBox() #kutu tanımlaması
        self.add(self.anakutu)
        self.connection = settings.connection

        main_up = Gtk.HBox()
        self.anakutu.pack_start(main_up,0,0,2)
        main_mid = Gtk.HBox()
        self.anakutu.pack_start(main_mid,0,0,2)
        main_down = Gtk.HBox()
        self.anakutu.pack_start(main_down,0,0,2)

        left_rpy = Gtk.VBox()
        main_mid.pack_start(left_rpy,0,0,2)

        rpy = Gtk.VBox()#-------roll pitch yaw-------
        main_mid.pack_start(rpy,0,0,2)

        right_rpy = Gtk.VBox()
        main_mid.pack_start(right_rpy,0,0,2)

        up = Gtk.Box()
        rpy.pack_start(up,0,0,2)

        down = Gtk.Box()
        rpy.pack_start(down,0,0,2)

        self.pitch = pitch.Pitch()
        down.pack_start(self.pitch,0,1,2)

        self.yaw = yaw.Yaw()
        down.pack_start(self.yaw,0,1,2)

        self.roll = roll.Roll()
        down.pack_start(self.roll,0,1,2)

        self.current = current.Current()
        up.pack_start(self.current,0,1,2)

        self.velo = velo.Velo()
        up.pack_start(self.velo,0,1,2)

        self.battery = battery.Battery()
        up.pack_start(self.battery,0,1,2)

        self.con_but = Gtk.Button("clicked",label = "DISCONNECTED")
        self.con_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("red"))

        self.arm_but = Gtk.Button("clicked",label = "DISARMED")
        self.arm_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("red"))

        if self.connection != None:
            if self.connection.arm:
                self.arm_but.set_label("ARMED")
                #rengi yeşil yapma
                self.arm_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("green"))
            else:
               self.arm_but.set_label("DISARMED")
               #rengi kırmızı yapma
               self.arm_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("red"))

        self.arm_but.connect("clicked", self.arm_c)
        main_up.pack_start(self.arm_but,0,0,2)
        self.con_but.connect("clicked", self.is_connected)
        main_up.pack_start(self.con_but,0,0,2)

        mode = Gtk.Box()
        main_up.pack_start(mode,0,0,2)

        paises=Gtk.ListStore(int,str)
        paises.append([1,"AUTO"])
        paises.append([2,"GUIDED"])
        
        self.mode_combo=Gtk.ComboBox.new_with_model_and_entry(paises)
        self.mode_combo.set_entry_text_column(1)
        mode.pack_start(self.mode_combo,0,0,2)

        self.mode_but = Gtk.Button("clicked",label = "send mode")
        self.mode_but.connect("clicked", self.mode_c)
        mode.pack_start(self.mode_but,0,0,2)

        mid = Gtk.VBox()#-------mid-------
        main_down.pack_start(mid,0,0,10)

        up_ = Gtk.Box()
        mid.pack_start(up_,0,0,2)

        cam = Gtk.Box()
        mid.pack_start(cam,0,0,2)

        bar = Gtk.VBox()
        mid.pack_start(bar,0,0,2)

        waypoint_1 = wp.Waypoint("seq","cur","frame","com",
                 "p_1","p_2","p_3","p_4",
                 "ln_x","ln_y","ln_z","auto")

        waypoint_2 = wp.Waypoint(1)
        waypoint_3 = wp.Waypoint(2)
        waypoint_4 = wp.Waypoint(3)


        wp_1 = Gtk.HBox()
        bar.pack_start(wp_1,1,1,2)
        for i in waypoint_1.ent_list:
            wp_1.pack_start(i,0,0,2)

        wp_2 = Gtk.HBox()
        bar.pack_start(wp_2,0,0,2)
        for k in waypoint_2.ent_list:
            wp_2.pack_start(k,0,0,2)

        wp_3 = Gtk.HBox()
        bar.pack_start(wp_3,0,0,2)
        for k in waypoint_3.ent_list:
            wp_3.pack_start(k,0,0,2)

        wp_4 = Gtk.HBox()
        bar.pack_start(wp_4,0,0,2)
        for k in waypoint_4.ent_list:
            wp_4.pack_start(k,0,0,2)

#######################################indictor###############

        indictor_b_1 = Gtk.VBox()
        right_rpy.pack_start(indictor_b_1,0,0,10)

        indictor_b_l_1_up = Gtk.Box()
        indictor_b_1.pack_start(indictor_b_l_1_up ,0,0,10)

        indictor_b_l_1_mid = Gtk.Box()
        indictor_b_1.pack_start(indictor_b_l_1_mid ,0,0,10)

        indictor_b_l_1_down = Gtk.Box()
        indictor_b_1.pack_start(indictor_b_l_1_down ,0,0,10)

        self.indicator_1 = indi.Indicator(40,200)
        indictor_b_l_1_mid.pack_start(self.indicator_1,1,0,5)

        self.label_1_ind_up = Gtk.Label()
        self.label_1_ind_up.set_text("yukseklik")
        indictor_b_l_1_up.pack_start(self.label_1_ind_up,1,1,5)

        self.label_1_ind_down = Gtk.Label()
        self.label_1_ind_down.set_text("100")
        indictor_b_l_1_down.pack_start(self.label_1_ind_down,1,1,5)
        
#########################################################################

        down_ = Gtk.Box()
        mid.pack_start(down_,0,0,10)

        right = Gtk.VBox()#-------right-------
        self.anakutu.pack_start(right,0,0,10)

        self.data_list = []

        data_1 = Gtk.Box()
        label_1 = Gtk.Label()
        label_1.set_text("Try")
        self.data_list.append(data_1)
        self.data_list.append(label_1)
        data_2 = Gtk.Box()
        label_2 = Gtk.Label()
        label_2.set_text("Try")
        self.data_list.append(data_2)
        self.data_list.append(label_2)
        data_3 = Gtk.Box()
        label_3 = Gtk.Label()
        label_3.set_text("Try")
        self.data_list.append(data_3)
        self.data_list.append(label_3)
        data_4 = Gtk.Box()
        label_4 = Gtk.Label()
        label_4.set_text("Try")
        self.data_list.append(data_4)
        self.data_list.append(label_4)

        self.frame = Gtk.Frame(label = "deneme")
        self.frame.set_label_align(0.5,0.5)

        #stupit_box = Gtk.Button("clicked",label = "deneme")
        #mid.pack_start(stupit_box,0,0,10)

        x = 0
        for data in self.data_list:
            if x % 2 == 0: # data
                right.pack_start(data,True,True,0)
            else:#label
                self.data_list[x-1].pack_start(data, True, True, 0)
            x+=1

    def change_label(self,label_c,label_n = ""):
        label_n = str(label_n)
        x = 0
        for data in self.data_list:
            if x % 2 == 1 and (label_c*2-1) == x: # label
                self.data_list[x].set_text(label_n)
            x+=1

    def arm_c(self,type):
        if self.connection != None:
            if self.connection.arm == 0:
                self.connection.send_arm(1)#arm eder
                self.arm_but.set_label("ARMED")
                #rengi yeşil yap
                self.arm_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("green"))
            elif self.connection.arm == 1:
                self.connection.send_arm(0)#disarm eder
                self.arm_but.set_label("DISARMED")
                #rengi kırmızı yap
                self.arm_but.modify_fg(Gtk.StateType.NORMAL, Gdk.color_parse("red"))

    def mode_c(self,type):
        if self.connection != None:
            c_iter = self.mode_combo.get_active_iter()
            if c_iter is not None:
                mode = self.mode_combo.get_model()
                mode_str = mode[c_iter][1]
                print("\n",mode_str,"\n")
                self.connection.set_mode(str(mode_str))
    def is_connected(self,type):
        if self.connection == None:
            settings.connection = connect.Connect()
            self.connection = settings.connection
        else:
            settings.connection = None
            self.connection = settings.connection
    def loop():
        global connection

        #msg = connection.take_message()
        #print("\n \n")
        #print(msg)

        battery_r=0
        roll=0
        pitch=0
        yaw=0
        air_velo=0
        altitude=0

    #------------bağlantıdan değişkenleri alma
        if connection != None:
            battery_r = connection.battery()
            r_p_y = connection.roll_pitch_yaw()
            altitude = int(connection.alt())*2
            roll = r_p_y[0]
            pitch = r_p_y[1]
            yaw = r_p_y[2]
            velo = connection.speeds()
            air_velo = int(velo[0])*4
            ground_velo = int(velo[1])*4
        #print(roll,pitch,yaw,air_velo,altitude,battery_r)

        #print(connection.mission().seq)

    #------------------------------------------
        ind_1_var = altitude
        window.indicator_1.pos[1] = int(ind_1_var)
        window.indicator_1.queue_draw()
        window.label_1_ind_down.set_text(str(ind_1_var))

    #---------değişkenleri çizdirme----------------------

        window.current.cursor_angle = -altitude
        window.current.queue_draw()

        window.roll.cursor_angle = roll*57.2957795
        window.roll.queue_draw()

        window.pitch.cursor_angle = pitch*57.2957795
        window.pitch.queue_draw()

        window.yaw.back_angle = yaw*57.2957795#radyan to degree
        window.yaw.queue_draw()

        window.battery.cursor_angle = battery_r*18/10
        window.battery.queue_draw()

        window.velo.cursor_angle = air_velo
        window.velo.queue_draw()
        
    #-----------------------------------------------------

        #label_count = int(len(window.data_list)/2)
        #for i in range(0,label_count+1):
        #    rand_say = random.randint(0,100)
        #    window.change_label(i,"%" + str(rand_say))

        return True

