import gi, calendar, datetime
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, Gdk

#ent_1.set_text("0")

class Waypoint():
    def __init__(self,ln_seq = "",ln_cur="",ln_frame="",ln_command="",
    	         ln_param1="",ln_param2="",ln_param3="",ln_param4="",
    	         ln_x="",ln_y="",ln_z="",ln_autocontinue=""):
        self.size = 5
        self.ent_list = []
        #ln_seq,ln_cur,ln_frame,ln_command,ln_param1,ln_param2,ln_param3,ln_param4
        #ln_x,ln_y,ln_z,ln_autocontinue
        #ln_seq
        ln_seq_box = Gtk.HBox()
        ln_seq_ent = Gtk.Entry()
        ln_seq_ent.set_width_chars(self.size)
        ln_seq_ent.set_text(str(ln_seq))
        self.ent_list.append(ln_seq_ent)
        #ln_cur
        ln_cur_box = Gtk.HBox()
        ln_cur_ent = Gtk.Entry()
        ln_cur_ent.set_width_chars(self.size)
        ln_cur_ent.set_text(str(ln_cur))
        self.ent_list.append(ln_cur_ent)
        #ln_frame
        ln_frame_box = Gtk.HBox()
        ln_frame_ent = Gtk.Entry()
        ln_frame_ent.set_width_chars(self.size)
        ln_frame_ent.set_text(str(ln_frame))
        self.ent_list.append(ln_frame_ent)
        #ln_command
        ln_com_box = Gtk.HBox()
        ln_com_ent = Gtk.Entry()
        ln_com_ent.set_width_chars(self.size)
        ln_com_ent.set_text(str(ln_command))
        self.ent_list.append(ln_com_ent)
        #ln_param1
        ln_par1_box = Gtk.HBox()
        ln_par1_ent = Gtk.Entry()
        ln_par1_ent.set_width_chars(self.size)
        ln_par1_ent.set_text(str(ln_param1))
        self.ent_list.append(ln_par1_ent)
        #ln_param2
        ln_par2_box = Gtk.HBox()
        ln_par2_ent = Gtk.Entry()
        ln_par2_ent.set_width_chars(self.size)
        ln_par2_ent.set_text(str(ln_param2))
        self.ent_list.append(ln_par2_ent)
        #ln_param3
        ln_par3_box = Gtk.HBox()
        ln_par3_ent = Gtk.Entry()
        ln_par3_ent.set_width_chars(self.size)
        ln_par3_ent.set_text(str(ln_param3))
        self.ent_list.append(ln_par3_ent)
        #ln_param4
        ln_par4_box = Gtk.HBox()
        ln_par4_ent = Gtk.Entry()
        ln_par4_ent.set_width_chars(self.size)
        ln_par4_ent.set_text(str(ln_param4))
        self.ent_list.append(ln_par4_ent)
        #ln_x
        ln_x_box = Gtk.HBox()
        ln_x_ent = Gtk.Entry()
        ln_x_ent.set_width_chars(self.size)
        ln_x_ent.set_text(str(ln_x))
        self.ent_list.append(ln_x_ent)
        #ln_y
        ln_y_box = Gtk.HBox()
        ln_y_ent = Gtk.Entry()
        ln_y_ent.set_width_chars(self.size)
        ln_y_ent.set_text(str(ln_y))
        self.ent_list.append(ln_y_ent)
        #ln_z
        ln_z_box = Gtk.HBox()
        ln_z_ent = Gtk.Entry()
        ln_z_ent.set_width_chars(self.size)
        ln_z_ent.set_text(str(ln_z))
        self.ent_list.append(ln_z_ent)
        #ln_autocontinue
        ln_aut_cont_box = Gtk.HBox()
        ln_aut_cont_ent = Gtk.Entry()
        ln_aut_cont_ent.set_width_chars(self.size)
        ln_aut_cont_ent.set_text(str(ln_autocontinue))
        self.ent_list.append(ln_aut_cont_ent)