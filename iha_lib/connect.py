from pymavlink import mavutil
from pymavlink import mavwp
from pymavlink.quaternion import QuaternionBase
import time
import math
import os

class Connect():
    def __init__(self,protocol = "udp",loc = "localhost", port = "14551"):

        self.connection = mavutil.mavlink_connection(str(protocol)+':'+str(loc) +':'+str(port)) # dinlemek için udp port bağlantısı
        #is hearthbeat timeouted
        self.is_plane_exist = self.connection.wait_heartbeat(timeout = 5)

        print("Heartbeat from system (system %u component %u)" %
        (self.connection.target_system, self.connection.target_component))
        if self.is_plane_exist != None:
            self.wp = mavwp.MAVWPLoader()
            self.mode = "Default"
            print(mavutil.mavlink.MAV_MODE_FLAG_SAFETY_ARMED)
            self.arm = 0
            #self.global_pos()
            #self.send_arm(1)
            self.uploadmission('mission.txt')
            self.set_mode("AUTO")

    def take_message(self,msg_type = None):

        #self.connection.mav.ping_send(int(time.time() * 1e6),0,0,0)
        if msg_type != None:
            msg = self.connection.recv_match(type = msg_type, blocking=True)
        else:
            msg = self.connection.recv_match(blocking=True)
        return msg

    def send_arm(self,bool_arm = 1):
        self.connection.mav.command_long_send(self.connection.target_system, self.connection.target_component,
        mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM,0 ,bool_arm,0,0,0,0,0,0)
        #print(self.take_message("COMMAND_ACK"))#4 failed 2 rejected
        if not(self.take_message("COMMAND_ACK").result):
            print("arm command recieved")
            self.arm = bool_arm
            if bool_arm==1:
                self.connection.motors_armed_wait()
        else:
            print("arm command is not recieved")

    def take_off(self,altitude = 20):
        self.connection.mav.command_long_send(self.connection.target_system, self.connection.target_component,
        mavutil.mavlink.MAV_CMD_NAV_TAKEOFF,0 ,0 ,0 ,0 ,0 ,self.latitude+100 ,self.longitude+100 ,altitude)
        #print(self.take_message("COMMAND_ACK"))
        if not(self.take_message("COMMAND_ACK").result):
            print("take off command recieved")
        else:
            print("take off command is not recieved")

    def waypoint(self,altitude):
        self.connection.mav.command_long_send(self.connection.target_system, self.connection.target_component,
            mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,0 ,0 ,0 ,0 ,0 ,self.latitude ,self.longitude ,altitude)

    def move(self,x,y,z):
        self.connection.mav.send(mavutil.mavlink.MAVLink_set_position_target_local_ned_message(10,self.connection.target_system,
        self.connection.target_component, mavutil.mavlink.MAV_FRAME_LOCAL_NED,int(0b110111111000) ,x ,y ,z ,0,0,0,0,0,0,0,0))
        print(self.take_message("COMMAND_ACK"))


    def set_mode(self,mode):
        # Get mode ID
        mode_id = self.connection.mode_mapping()[mode]
        # Set new mode
        self.connection.mav.set_mode_send(self.connection.target_system,mavutil.mavlink.MAV_MODE_FLAG_CUSTOM_MODE_ENABLED,mode_id)
        #print(self.take_message("COMMAND_ACK"))
        if not(self.take_message("COMMAND_ACK").result):
            print("set mode command recieved")
            self.mode = mode
        else:
            print("set mode command is not recieved")

    def set_target_attitude(self,roll, pitch, yaw):
        boot_time = time.time()
        print("here")
        self.connection.mav.set_attitude_target_send(
            int(1e3 * (time.time() - boot_time)), # ms since boot
            self.connection.target_system, self.connection.target_component,
            # allow throttle to be controlled by depth_hold mode
            mavutil.mavlink.ATTITUDE_TARGET_TYPEMASK_THROTTLE_IGNORE,
            # -> attitude quaternion (w, x, y, z | zero-rotation is 1, 0, 0, 0)
            QuaternionBase([math.radians(angle) for angle in (roll, pitch, yaw)]),
            0, 0, 0, 0 # roll rate, pitch rate, yaw rate, thrust
        )
        if not(self.take_message("COMMAND_ACK").result):
            print("yaw command recieved")
        else:
            print("yaw command is not recieved")

    def yaw(self,speed):
        roll_angle = pitch_angle = 0
        for yaw_angle in range(0, 500, speed):
            self.set_target_attitude(roll_angle, pitch_angle, yaw_angle)
            time.sleep(0.1) # wait for a second

    def global_pos(self):
        global_pos = self.take_message("GLOBAL_POSITION_INT")
        self.latitude = global_pos.lat
        self.longitude = global_pos.lon
        return [self.latitude,self.longitude]

    def alt(self):
        altitude = self.take_message("TERRAIN_REPORT").current_height#yükseklik
        #print(altitude)
        return altitude

    def roll_pitch_yaw(self):
        msg = self.take_message("SIMSTATE")
        #print(msg)
        yaw = msg.yaw
        #print(yaw)
        roll = msg.roll
        #print(roll)
        pitch = msg.pitch
        #print(pitch)
        return [roll,pitch,yaw]

    def battery(self):
        battery_r = self.take_message("SYS_STATUS").battery_remaining
        return battery_r

    def wind(self):
        wind = self.take_message("WIND")
        return wind

    def vibration(self):
        vibration = self.take_message("VIBRATION")
        return vibration

    def pressure(self):
        pressure = self.take_message("SCALED_PRESSURE")
        return pressure

    def speeds(self):
        vfr_hud = self.take_message("VFR_HUD")
        airspeed = vfr_hud.airspeed
        groundspeed = vfr_hud.groundspeed
        return [airspeed,groundspeed]

    def mission(self):
        mission = self.take_message("MISSION_CURRENT")
        return mission

    def cmd_set_home(self,home_location, altitude):
        print('--- ', self.connection.target_system, ',', self.connection.target_component)
        self.connection.mav.command_long_send(
            self.connection.target_system, self.connection.target_component,
            mavutil.mavlink.MAV_CMD_DO_SET_HOME,
            1, # set position
            0, # param1
            0, # param2
            0, # param3
            0, # param4
            home_location[0], # lat
            home_location[1], # lon
            altitude) 

    def uploadmission(self,aFileName):
        home_location = None
        home_altitude = None
        aFileName = os.path.join(os.getcwd(),"iha_lib",str(aFileName))
        with open(aFileName) as f:
            for i, line in enumerate(f):
                if i==0:
                    if not line.startswith('QGC WPL 110'):
                        raise Exception('File is not supported WP version')
                else:   
                    linearray=line.split('\t')
                    ln_seq = int(linearray[0])
                    ln_current = int(linearray[1])
                    ln_frame = int(linearray[2])
                    ln_command = int(linearray[3])
                    ln_param1=float(linearray[4])
                    ln_param2=float(linearray[5])
                    ln_param3=float(linearray[6])
                    ln_param4=float(linearray[7])
                    ln_x=float(linearray[8])
                    ln_y=float(linearray[9])
                    ln_z=float(linearray[10])
                    ln_autocontinue = int(float(linearray[11].strip()))
                    if(i == 1):
                        home_location = (ln_x,ln_y)
                        home_altitude = ln_z
                    p = mavutil.mavlink.MAVLink_mission_item_message(
                            self.connection.target_system, self.connection.target_component,
                            ln_seq, ln_frame, ln_command,ln_current, ln_autocontinue,
                            ln_param1, ln_param2, ln_param3, ln_param4, ln_x, ln_y, ln_z)
                    self.wp.add(p)
                        
        self.cmd_set_home(home_location,home_altitude)
        msg = self.connection.recv_match(type = ['COMMAND_ACK'],blocking = True)
        print(msg)
        print('Set home location: {0} {1}'.format(home_location[0],home_location[1]))
        time.sleep(1)

        #send waypoint to airframe
        self.connection.waypoint_clear_all_send()
        self.connection.waypoint_count_send(self.wp.count())

        for i in range(self.wp.count()):
            msg = self.connection.recv_match(type=['MISSION_REQUEST'],blocking=True)
            print(msg)
            self.connection.mav.send(self.wp.wp(msg.seq))
            print('Sending waypoint {0}'.format(msg.seq))

