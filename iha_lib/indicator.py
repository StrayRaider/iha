import gi, os
gi.require_version('Gtk','3.0')
from gi.repository import Gtk, Gdk, GObject, GdkPixbuf

class Indicator(Gtk.DrawingArea):
    def __init__(self,size_x=10,size_y=100):
        Gtk.DrawingArea.__init__(self)
        self.size = (size_x,size_y)
        self.set_size_request(*self.size)#draw area size ataması yapıldı
        self.pos = [0,0]#bar yüksekliği 0 max
        self.s_pos = (0,0)
        self.connect("draw", self.draw_all)
        self.queue_draw()#ekrana yazdırma fonksiyoun

    def draw_all(self,widget,cr):
        img_path = os.path.expanduser("./assets/indica_fg.png")
        self.image_1 = GdkPixbuf.Pixbuf.new_from_file(img_path)#,self.image_size,self.image_size)
        Gdk.cairo_set_source_pixbuf(cr,self.image_1,self.pos[0],self.pos[1])
        cr.paint()

        img_path = os.path.expanduser("./assets/indica_bg.png")
        self.image_2 = GdkPixbuf.Pixbuf.new_from_file(img_path)
        Gdk.cairo_set_source_pixbuf(cr,self.image_2,self.s_pos[0],self.s_pos[1])
        cr.paint()

